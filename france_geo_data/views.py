import re

from django.conf              import settings
from rest_framework           import status
from geopy.geocoders          import GoogleV3
from djgeojson.serializers    import Serializer
from france_geo_data.models   import Commune
from france_geo_data.models   import Departement
from rest_framework.views     import APIView
from rest_framework.response  import Response
from rest_framework.renderers import JSONRenderer
from rest_framework.renderers import TemplateHTMLRenderer
from django.contrib.gis.geoip import GeoIP
from django.core.exceptions   import SuspiciousOperation
from django.contrib.gis.geos  import Point


USE_X_FORWARDED_FOR = getattr(settings, 'USE_X_FORWARDED_FOR', False)
ip_validation_re = re.compile(r"^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$")


class Departements(APIView):
    renderer_classes = (JSONRenderer,)

    def get(self, request, id=None):
        source     = []
        serializer = Serializer()
        properties = ['id', 'nom']

        if id is not None:
            source = Commune.objects.filter(canton__arrondissement__departement=id)
            properties.append('zip')
        else:
            source = Departement.objects.all()

        return Response(serializer.serialize(source, properties=properties, geometry_field='geom', srid=4326))


class IPGeolocator(APIView):
    renderer_classes = (JSONRenderer,)

    def get_ip_address(self, request):
        if USE_X_FORWARDED_FOR and 'HTTP_X_FORWARDED_FOR' in request.META:
            address = request.META['HTTP_X_FORWARDED_FOR'].split(",", 1)[0].strip()
        elif 'REMOTE_ADDR' in request.META:
            address = request.META['REMOTE_ADDR']
        else:
            address = "127.0.0.1"
        if not ip_validation_re.match(address):
            raise SuspiciousOperation("Invalid client IP: %s" % address)
        return address

    def get(self, request):
        try:
            geo_ip     = GeoIP()

            ip      = self.get_ip_address(request)
            city    = geo_ip.city(ip)
            lat_lon = geo_ip.lat_lon(ip)
            commune = Commune.objects.get(geom__contains=Point(lat_lon[1], lat_lon[0], srid=4326))

            return Response({
                'ip'         : ip,
                'city'       : city,
                'lat_lon'    : lat_lon,
                'commune_zip': commune.zip,
                'commune_nom': commune.nom,
            })

        except Exception as e:
            return Response({
                'ip'     : ip          ,
                'error'  : e.message   ,
                'request': request.DATA,
            }, status.HTTP_400_BAD_REQUEST)


class AddressGeolocator(APIView):
    renderer_classes = (JSONRenderer,)

    def post(self, request):
        try:
            g_address, (g_latitude, g_longitude) = GoogleV3().geocode(request.DATA['address'])
            g_commune = Commune.objects.get(geom__contains=Point(g_longitude, g_latitude, srid=4326))

            return Response({
                'google_address'       : g_address  ,
                'google_latitude'      : g_latitude ,
                'google_longitude'     : g_longitude,
                'google_commune_zip'   : g_commune.zip,
                'google_commune_nom'   : g_commune.nom,
            })

        except Exception as e:
            return Response({
                'error'  : e.message   ,
                'request': request.DATA,
            }, status.HTTP_400_BAD_REQUEST)


class Index(APIView):
    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request):
        return Response({}, template_name='index.html')


class CommuneToZip(APIView):
    renderer_classes = (JSONRenderer,)

    def get(self, request):
        if request.is_ajax():
            query    = request.GET.get('term', '')
            results  = []
            if query.isdigit():
                communes = Commune.objects.filter(zip__icontains=query ).order_by('zip')
            else:
                communes = Commune.objects.filter(nom__icontains=query ).order_by('nom')
            for commune in communes:
                commune_serialization = u"{0} - {1}".format(commune.zip, commune.nom)
                commune_json          = {}
                commune_json['id'   ] = commune.id
                commune_json['label'] = commune_serialization
                commune_json['value'] = commune_serialization
                results.append(commune_json)
            return Response(results)
        else:
            return Response('fail')
