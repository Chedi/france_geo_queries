$(function() {
	window.main_map              = null                                    ;
	window.communes_layer        = null                                    ;
	window.departements_layer    = null                                    ;
	window.departements_control  = null                                    ;

	window.GEOJSON_FORMAT        = new OpenLayers.Format.GeoJSON()         ;
	window.PROJCTION_EPSG_4326   = new OpenLayers.Projection("EPSG:4326"  );
	window.PROJCTION_EPSG_900913 = new OpenLayers.Projection("EPSG:900913");

	var map_reset_panel = new OpenLayers.Control.Panel({
		displayClass: 'map_reset_panel'
	});

	var reset_map = new OpenLayers.Control.Button({
		displayClass: "reset_map",
		trigger     : function(){
			for(i=2;i<main_map.layers.length;++i)
				main_map.layers[i].setVisibility(false);
			main_map.layers[1].setVisibility(true);
			departements_control.unselectAll();
			main_map.zoomToExtent(main_map.layers[1].getDataExtent());
	}});

	window.pink_map_style = new OpenLayers.StyleMap({
		"default": new OpenLayers.Style({
			fillColor    : "#e596a3",
			fillOpacity  : 0.5      ,
			strokeColor  : "#9b9b9b",
			strokeWidth  : 1,
			strokeOpacity: 1,
		}),
		"temporary": new OpenLayers.Style({
			fillColor    : "#e596a3",
			fillOpacity  : 1        ,
			strokeColor  : "#9b9b9b",
			strokeWidth  : 1        ,
			strokeOpacity: 1        ,
		}),
		"select": new OpenLayers.Style({
			fillColor    : "#e596a3",
			fillOpacity  : 1        ,
			strokeColor  : "#9b9b9b",
			strokeWidth  : 1        ,
			strokeOpacity: 1        ,
	})});

	window.azure_map_style = new OpenLayers.StyleMap({
		"default": new OpenLayers.Style({
			fillColor    : "#9bbff8",
			fillOpacity  : 0.2      ,
			strokeColor  : "#9b9b9b",
			strokeWidth  : 1        ,
			strokeOpacity: 1        ,
		}),
		"temporary": new OpenLayers.Style({
			fillColor    : "#9bbff8",
			fillOpacity  : 1        ,
			strokeColor  : "#9b9b9b",
			strokeWidth  : 1        ,
			strokeOpacity: 1        ,
		}),
		"select": new OpenLayers.Style({
			fillColor    : "#9bbff8",
			fillOpacity  : 1        ,
			strokeColor  : "#9b9b9b",
			strokeWidth  : 1        ,
			strokeOpacity: 1        ,
	})});


	$("#ip_geo_localization"     ).click(function(event){ ip_geo_localization     () });
	$("#address_geo_localization").click(function(event){ address_geo_localization() });

	$("#commune_name").autocomplete({
		source   : commune_to_zip_url,
		minLength: 3                 ,
	});

	main_map = new OpenLayers.Map ("map",{
		projection       : PROJCTION_EPSG_900913,
		displayProjection: PROJCTION_EPSG_4326  ,
		controls         : [
			new OpenLayers.Control.Navigation(),
			new OpenLayers.Control.PanZoomBar(),
	]});

	main_map.addLayer (new OpenLayers.Layer.Google("Google Streets", {'sphericalMercator': true}));
	map_reset_panel.addControls([reset_map]);
	main_map.addControl(map_reset_panel);
	load_departements();
});

function get_feature_qtip(event) {
	return $('#'+event.feature.geometry.components[0].id).qtip({
		overwrite: false,
		style    : { classes: 'qtip-light qtip-shadow qtip-rounded' },
		content  : { 
			text : event.feature.attributes.nom,
			title: event.feature.attributes.zip,
		},
		show      : { 
			ready : true,
			effect: function() {
				$(this).fadeTo(500, 1); 
}}})}

function initialize_qtip(layer) {
	activate_layer_control(new OpenLayers.Control.SelectFeature(layer, {
		hover         : true       ,
		highlightOnly : true       ,
		renderIntent  : "temporary",
		eventListeners: {
			featurehighlighted  : function(event) { get_feature_qtip(event).qtip('show'); },
			featureunhighlighted: function(event) { get_feature_qtip(event).qtip('hide'); },
}}))}

function load_departement_communes(departement){
	var layer_name = "communes_"+departement;
	if(typeof(main_map.getLayersByName(layer_name)[0]) == "undefined"){
		$.ajax({
			url : departements_url + departement,
		})
		.done(function(features_collection) {
			commune_layer = create_layer_from_geojson(layer_name, features_collection, pink_map_style);
			switch_to_departement_communes(commune_layer);
			activate_layer_control(new OpenLayers.Control.SelectFeature([
				departements_layer,
				commune_layer     ,
				],{
				hover   : false,
				clickout: false,
				multiple: false,
			}))
			commune_layer     .events.on({"featureunselected": commune_select     });
			departements_layer.events.on({"featureunselected": departement_select });

            commune_layer     .unselectAll();
            departements_layer.unselectAll();
		})
		.fail(function() {
			alert("loading departement failed");
		});
	} else {
		switch_to_departement_communes(main_map.getLayersByName(layer_name)[0]);
}}

function switch_to_departement_communes(layer){
	for(i=2;i<main_map.layers.length;++i)
		main_map.layers[i].setVisibility(false);
	layer.setVisibility(true);
	main_map.zoomToExtent(layer.getDataExtent());
}

function create_layer_from_geojson(layer_name, layer_features, map_style) {
	layer = new OpenLayers.Layer.Vector(layer_name, {
		styleMap        : map_style                 ,
		projection      : main_map.displayProjection,
		preFeatureInsert: function(feature) {
			feature.geometry.transform(PROJCTION_EPSG_4326, main_map.getProjection());
	}});

	initialize_qtip(layer);
	main_map.addLayer(layer);
	layer.addFeatures(GEOJSON_FORMAT.read(layer_features));
	return layer;
}

function activate_layer_control(control) {
	main_map.addControl(control);
	control.activate();
}

function address_geo_localization() {
	address = $('#address_to_commune')[0].value;

	$.ajax({
		url : address_geolocator_url,
		type: "POST",
		data: {
			'address': address,
	}})
	.done(function(data) {
		$('#address_result')[0].textContent = JSON.stringify(data);
	})
	.fail(function() {
		alert("address geolocalization failed");
})}

function ip_geo_localization() {
	$.ajax({
		url : ip_geolocator_url,
	})
	.done(function(data) {
		$('#address_result')[0].textContent = JSON.stringify(data);
	})
	.fail(function() {
		alert("address ip geolocalization failed");
})}

function load_departements() {
	$.ajax({
		url: departements_url,
	})
	.done(function(features_collection) {
		departements_layer = create_layer_from_geojson("Departements", features_collection, azure_map_style);
		main_map.zoomToExtent(departements_layer.getDataExtent());

		window.departements_control = new OpenLayers.Control.SelectFeature(departements_layer, {
			hover   : false,
			clickout: false,
			multiple: false,
			onSelect: departement_select
		});
		activate_layer_control(departements_control);
	})
	.fail(function() {
		alert( "loading departements failed" );
})}


function commune_select(event) {
	console.log(event.feature);
	$('#address_result')[0].textContent = JSON.stringify(event.feature.attributes);
}

function departement_select(event) {
	console.log(event.attributes);
	departements_control.unselectAll();
	if(typeof(event.fid) === "undefined")
		load_departement_communes(event.feature.fid);
	else
		load_departement_communes(event.fid);
}
