from django import conf

from django.conf.urls import url
from django.contrib   import admin
from django.conf.urls import include
from django.conf.urls import patterns

from france_geo_data.views  import *

admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^admin/'                       , include(admin.site.urls)                              ),

    url(r'^$'                            , Index            .as_view(), name='index'             ),

    url(r'^geo/departements/$'           , Departements     .as_view(), name='departements'      ),
    url(r'^geo/departements/(?P<id>\d+)$', Departements     .as_view(), name='departements_id'   ),

    url(r'^geo/ip_geolocator/$'          , IPGeolocator     .as_view(), name='ip_geolocator'     ),
    url(r'^geo/commune_to_zip/$'         , CommuneToZip     .as_view(), name='commune_to_zip'    ),
    url(r'^geo/address_geolocator/$'     , AddressGeolocator.as_view(), name='address_geolocator'),
)

if conf.settings.DEBUG:
    urlpatterns = patterns(
        '',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': conf.settings.MEDIA_ROOT, 'show_indexes': True}),
        url(r'', include('django.contrib.staticfiles.urls')),
    ) + urlpatterns
